#include <algorithm>
#include <iostream>

int main() {
    unsigned short T;
    std::cin >> T;
    while (T--) {
        unsigned short N, K;
        std::cin >> N >> K;

        std::string series;
        std::cin >> series;

        auto digit = [&series](std::size_t i) { return series[i] - '0'; };

        unsigned long long max_product = 0;
        unsigned long long current_product = 1;

        for (std::size_t i = 0, current_digits = 0; i < series.size(); i++) {
            if (digit(i) == 0) {
                current_product = 1;
                current_digits = 0;
            } else {
                current_product *= digit(i);
                if (current_digits < K) {
                    current_digits++;
                } else {
                    current_product /= digit(i - K);
                }

                if (current_digits == K && current_product > max_product) {
                    max_product = current_product;
                }
            }
        }

        std::cout << max_product << std::endl;
    }
}
