#include <algorithm>
#include <iostream>
#include <vector>

unsigned long long highest_prime_factor_of(unsigned long long N) {
    unsigned long long max = N;
    unsigned long long remaining = N;

    unsigned long long factor = 2;
    while (factor * factor <= remaining) {
        if (remaining % factor == 0) {
            remaining = remaining / factor;
        } else {
            factor++;
        }
    }

    max = remaining;
    return max;
}

int main() {
    unsigned T;
    std::cin >> T;
    while (T--) {
        unsigned long long N;
        std::cin >> N;
        std::cout << highest_prime_factor_of(N) << std::endl;
    }
    return 0;
}
