#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

unsigned long long sum_of_n_natural_numbers(unsigned long N) {
    unsigned long long n = N;
    return n * (n + 1) / 2;
}

int main() {
    unsigned T;
    std::cin >> T;
    while (T--) {
        unsigned long N;
        std::cin >> N;
        const auto sum_of_multiples_of_3 = 3 * sum_of_n_natural_numbers((N - 1) / 3);
        const auto sum_of_multiples_of_5 = 5 * sum_of_n_natural_numbers((N - 1) / 5);
        const auto sum_of_multiples_of_3_and_5 = 15 * sum_of_n_natural_numbers((N - 1) / 15);
        const auto result = sum_of_multiples_of_3 + sum_of_multiples_of_5 - sum_of_multiples_of_3_and_5;
        std::cout << result << std::endl;
    }
    return 0;
}
