#include <algorithm>
#include <array>
#include <iostream>

template <typename T>
unsigned long largest_product_sequence(T&& sequence, std::size_t N, std::size_t K) {
    unsigned long max_product = 0;
    for (std::size_t i = 0, current_product = 1, current_numbers = 0; i < N; i++) {
        const auto n = sequence(i);
        if (n == 0) {
            current_product = 1;
            current_numbers = 0;
        } else {
            current_product *= sequence(i);
            if (current_numbers < K) {
                current_numbers++;
            } else {
                current_product /= sequence(i - K);
            }

            if (current_numbers == K && current_product > max_product) {
                max_product = current_product;
            }
        }
    }

    return max_product;
}

int main() {
    static constexpr auto N = 20;
    std::array<std::array<unsigned short, N>, N> grid;

    for (auto i = 0; i < N; i++) {
        for (auto j = 0; j < N; j++) {
            std::cin >> grid[i][j];
        }
    }

    static constexpr auto K = 4;

    unsigned long max = 0;

    // Horizontal sequences
    for (std::size_t i = 0; i < N; i++) {
        max = std::max(max, largest_product_sequence([&grid, i](std::size_t j) { return grid[i][j]; }, N, K));
    }

    // Vertical sequences
    for (std::size_t j = 0; j < N; j++) {
        max = std::max(max, largest_product_sequence([&grid, j](std::size_t i) { return grid[i][j]; }, N, K));
    }

    // Main diagonals
    max = std::max(max, largest_product_sequence([&grid](std::size_t i) { return grid[i][i]; }, N, K));
    max = std::max(max, largest_product_sequence([&grid](std::size_t i) { return grid[i][N - 1 - i]; }, N, K));

    // Partial diagonals
    for (std::size_t diff = 1; diff < N - K + 1; diff++) {
        max = std::max(max, largest_product_sequence([&grid, diff](std::size_t i) { return grid[i][i + diff]; }, N - diff, K));
        max = std::max(max, largest_product_sequence([&grid, diff](std::size_t i) { return grid[i + diff][i]; }, N - diff, K));
        max = std::max(max, largest_product_sequence([&grid, diff](std::size_t i) { return grid[i][N - 1 - i - diff]; }, N - diff, K));
        max = std::max(max, largest_product_sequence([&grid, diff](std::size_t i) { return grid[N - 1 - i - diff][i]; }, N - diff, K));
    }

    std::cout << max << std::endl;
}
