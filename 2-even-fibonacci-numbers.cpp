#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

// a --> even
// b --> odd
// a + b --> odd
// a + 2b --> even
// 2a + 3b --> odd
// 3a + 5b --> odd
// 5a + 8b --> even
// 8a + 13b --> odd
// 13a + 21b --> odd
//
// next even = c
// c = a + 2b
// b = (c - a) / 2
//
// next even after c = 5a + 8b
// = 5a + 8*(c-a)/2
// = a + 4c

unsigned long long even_fib_sum(unsigned long long N) {
    unsigned long long a = 0;
    unsigned long long c = 2;
    unsigned long long sum = 0;
    while (c < N) {
        sum += c;
        const auto prev_c = c;
        c = a + 4 * c;
        a = prev_c;
    }
    return sum;
}

int main() {
    unsigned T;
    std::cin >> T;
    while (T--) {
        unsigned long long N;
        std::cin >> N;
        std::cout << even_fib_sum(N) << std::endl;
    }
    return 0;
}
