#include <cmath>
#include <iostream>
#include <vector>

std::vector<unsigned long> list_of_primes;
unsigned long nthprime(unsigned short N) {
    if (list_of_primes.size() == 0) {
        list_of_primes.push_back(2);
        list_of_primes.push_back(3);
    }

    if (list_of_primes.size() >= N) {
        return list_of_primes[N - 1];
    }

    list_of_primes.reserve(N);

    for (unsigned long i = list_of_primes.back() + 2;; i += 2) {
        bool is_prime = true;
        for (auto p : list_of_primes) {
            unsigned long pred_sqr = p * p;
            if (pred_sqr > i) {
                break;
            } else if (i % p == 0) {
                is_prime = false;
                break;
            }
        }

        if (is_prime) {
            list_of_primes.push_back(i);
            if (list_of_primes.size() == N) {
                return i;
            }
        }
    }
}

int main() {
    unsigned T;
    std::cin >> T;
    while (T--) {
        unsigned N;
        std::cin >> N;
        std::cout << nthprime(N) << std::endl;
    }
    return 0;
}
