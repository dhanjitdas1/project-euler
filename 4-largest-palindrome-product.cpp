#include <iostream>

int main() {
    short T;
    std::cin >> T;

    auto palindrome = [](int a, int b, int c) {
        unsigned long a_ = a;
        unsigned long b_ = b;
        return 100001 * a_ + 10010 * b_ + 1100 * c;
    };

    while (T--) {
        unsigned long N;
        std::cin >> N;
        int a = N / 100000;
        int b = N / 10000 - (N / 100000) * 10;
        int c = N / 1000 - (N / 10000) * 10;

        if (palindrome(a, b, c) >= N) c--;

        while (a > 0) {
            while (b >= 0) {
                while (c >= 0) {
                    const auto N_by_11 = palindrome(a, b, c) / 11;
                    for (unsigned long k = 11; k * 11 <= 999; k++) {
                        if (N_by_11 % k == 0 && (N_by_11 / k) > 99 && (N_by_11 / k) < 1000) {
                            a = b = c = 0;
                            std::cout << N_by_11 * 11 << std::endl;
                            break;
                        }
                    }
                    c--;
                }
                b--;
                c = 9;
            }
            a--;
            b = 9;
        }
    }
    return 0;
}
