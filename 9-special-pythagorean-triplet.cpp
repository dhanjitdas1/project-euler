#include <cmath>
#include <iostream>
// a + b + c = N
// say X = abc
// a^2 + b^2 = c^
// => (a+b)^2 - c^2 = 2ab
// => (N - c)^2 -c^2 = 2ab
// => (N - 2c)N = 2ab
// => cN(N-2c) = 2X
// Above is a quadratic equation, need to maximize X.
// X is highest when c = N / 4
// a , b = (N -c +- sqrt(c^2 - N^2 + 2cN)) / 2
// Discriminant for a,b should be > 0 => c > (sqrt(2) - 1)*N
// also a < b < c; a < c => (N - c - sqrt(c^2 - N^2 + 2cN))/2 > 0 => N > 2c
// so (sqrt(2) - 1)*N < c < N/2
// Note N/4 < (sqrt(2) - 1)N => X decreases as c increases from (sqrt(2) - 1)N
// So, we start from c = (sqrt(2)-1)N
//
// Odd/Even checks
// a + b + c = N; (a + b)^2 - 2ab = c^2
// 2 | (a+b) => 2 | c => 2 | N
// 2 |- (a+b) => 2 |- c => 2 | N
// Hence N must be even.
//

long special_pythagorean_triplet(unsigned N) {
    long result = -1;
    if (N % 2 == 0) {
        for (unsigned c = (std::sqrt(2) - 1.0) * N + 0.5; c < N / 2; c++) {
            unsigned long n = N;
            unsigned long discriminant = (n + c) * c - (n - c) * n;
            unsigned value = std::sqrt(discriminant);
            if (value * value == discriminant) {
                result = c * n * (n - 2 * c) / 2;
                break;
            }
        }
    }
    return result;
}

int main() {
    unsigned T;
    std::cin >> T;

    while (T--) {
        unsigned N;
        std::cin >> N;
        std::cout << special_pythagorean_triplet(N) << std::endl;
    }
}
