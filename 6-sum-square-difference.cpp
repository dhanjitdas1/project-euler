#include <iostream>

unsigned long long difference_of_sum_of_squares_and_square_of_sum(unsigned N) {
    unsigned long long n = N;
    return (n * n * n - n) * (3 * n + 2) / 12;
}

int main() {
    unsigned T;
    std::cin >> T;
    while (T--) {
        unsigned int N;
        std::cin >> N;
        std::cout << difference_of_sum_of_squares_and_square_of_sum(N) << std::endl;
    }
    return 0;
}
