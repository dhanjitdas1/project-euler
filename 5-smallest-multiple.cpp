#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

std::vector<unsigned short> generate_list_of_primes(unsigned short N) {
    std::vector<unsigned short> list_of_primes;
    list_of_primes.reserve(N);
    list_of_primes.push_back(2);
    for (unsigned short i = 3; i <= N; i += 2) {
        if (!std::any_of(list_of_primes.cbegin(), list_of_primes.cend(), [i](unsigned short p) { return i % p == 0; })) {
            list_of_primes.push_back(i);
        }
    }
    return list_of_primes;
}

unsigned long smallest_multiple(unsigned short N, std::vector<unsigned short>& list_of_primes, bool precompute) {
    if (!precompute) list_of_primes = generate_list_of_primes(N);

    unsigned long result = 1;
    // const auto logN = std::log(N);
    for (unsigned short i = 0; i < list_of_primes.size() && list_of_primes[i] <= N; i++) {
        auto p = list_of_primes[i];
        while (p * list_of_primes[i] <= N) {
            p *= list_of_primes[i];
        }
        result *= p;

        /*
        unsigned short power_of_p = logN / std::log(p);
        result *= std::pow(p, power_of_p);
        */
    }

    return result;
}

template <typename T, typename U>
auto gcd(T a, U b) -> typename std::decay<decltype(a + b)>::type {
    typename std::decay<decltype(a + b)>::type t;

    while (b != 0) {
        t = b;
        b = a % b;
        a = t;
    }

    return a;
}

template <typename T, typename U>
auto lcm(T a, U b) -> typename std::decay<decltype(a + b)>::type {
    return a * b / gcd(a, b);
}

unsigned long smallest_multiple_using_gcd(unsigned short N) {
    unsigned long result = 1;
    for (unsigned short i = 2; i <= N; i++) {
        result = lcm(result, i);
    }
    return result;
}

int main() {
    unsigned short T;
    std::cin >> T;

    bool precompute = false;
    std::vector<unsigned short> list_of_primes = (precompute ? generate_list_of_primes(40) : std::vector<unsigned short>());
    while (T--) {
        unsigned short N;
        std::cin >> N;
        // std::cout << smallest_multiple(N, list_of_primes, precompute) << std::endl;
        std::cout << smallest_multiple_using_gcd(N) << std::endl;
    }
    return 0;
}
