#include <cmath>
#include <iostream>
#include <vector>

std::vector<unsigned long> list_of_primes;

unsigned long summation_of_primes(unsigned long N) {
    if (list_of_primes.size() == 0) {
        list_of_primes.push_back(2);
        list_of_primes.push_back(3);
    }

    unsigned long sum = 0;

    for (auto p : list_of_primes) {
        if (p > N) {
            return sum;
        } else {
            sum += p;
        }
    }

    list_of_primes.reserve(N / std::log(N));

    for (std::size_t i = list_of_primes.back() + 2; i <= N; i += 2) {
        bool is_prime = true;
        for (auto p : list_of_primes) {
            unsigned long pred_sqr = p * p;
            if (pred_sqr > i) {
                break;
            } else if (i % p == 0) {
                is_prime = false;
                break;
            }
        }

        if (is_prime) {
            sum += i;
            list_of_primes.push_back(i);
        }
    }

    return sum;
}

int main() {
    unsigned T;
    std::cin >> T;

    while (T--) {
        unsigned long N;
        std::cin >> N;
        std::cout << summation_of_primes(N) << std::endl;
    }
}
